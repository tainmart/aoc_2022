<?php

ini_set('memory_limit', '1024M');

$input = array_map(
    static function (string $line): array {
        [$sensorX, $sensorY, $beaconX, $beaconY] = sscanf($line, 'Sensor at x=%d, y=%d: closest beacon is at x=%d, y=%d');
        return [
            'sensor' => [$sensorX, $sensorY],
            'beacon' => [$beaconX,  $beaconY],
            'radius' => abs($sensorX - $beaconX) + abs($sensorY - $beaconY)
        ];
    },
    file($argv[1], FILE_IGNORE_NEW_LINES),
);
$sensors = array_column($input, 'sensor');
$beacons = array_column($input, 'beacon');

[$lineNumber, $boundingSize] = $argv[1] === 'example.txt' ? [10, 20] : [2_000_000, 4_000_000];

$line1 = array_reduce(
    $input,
    static function (array $line, array $input) use ($lineNumber): array {
        [$sensorX, $sensorY] = $input['sensor'];
        [$beaconX, $beaconY] = $input['beacon'];
        $radius = $input['radius'];

        if ($beaconY === $lineNumber) {
            $line[$beaconX] = 'B';
        }
        if ($sensorY === $lineNumber) {
            $line[$sensorX] = 'S';
        }

        $yDistance = abs($lineNumber - $sensorY);
        $xSize = $radius - $yDistance;

        $minX = $sensorX - ($xSize);
        $maxX = $sensorX + ($xSize);

        foreach (range($minX, $maxX) as $x) {
            if (isset($line[$x])) {
                continue;
            }
            $line[$x] = '#';
        }

        return $line;
    },
    []
);

$distressBeacon = [];
$lineNumber = $boundingSize;
while ($lineNumber--) {
    $lines = array_reduce(
        $input,
        static function (array $lines, array $input) use ($lineNumber, $boundingSize): array {
            [$sensorX, $sensorY] = $input['sensor'];
            $radius = $input['radius'];

            $yDistance = abs($lineNumber - $sensorY);
            $xLength = $radius - $yDistance;

            if ($sensorY > $lineNumber) {
                $xLength = $sensorY - $radius;
                if ($xLength > $lineNumber) {
                    return $lines;
                }
            } else {
                $xLength = $sensorY + $radius;
                if ($xLength < $lineNumber) {
                    return $lines;
                };
            }

            $diff = abs($lineNumber - $xLength);
            $start = max(0, ($sensorX - $diff));
            $end = min($boundingSize, $sensorX + $diff + 1);

            $lines[$start] = max($end - 1, ($lines[$start] ?? 0));
            return $lines;
        },
        []
    );

    ksort($lines);
    $cursor = 0;
    foreach ($lines as $min => $max) {
        if ($min - 1 > $cursor) {
            $distressBeacon = [$cursor + 1, $lineNumber];
            break 2;
        }
        $cursor = max($cursor, $max);
    }
}
[$distressBeaconX, $distressBeaconY] = $distressBeacon;

$solution1 = array_count_values($line1)['#'];
$solution2 = $distressBeaconX * 4_000_000 + $distressBeaconY;

echo "Solution 15-1: $solution1\n";
echo "Solution 15-2: $solution2\n";
