<?php

const SAND_START_POSITION = [500, 0];

$input = array_map(
    static fn (string $line): array => array_map(
        static fn (string $coordinates): array => explode(',', $coordinates),
        explode(' -> ', $line)
    ),
    file($argv[1], FILE_IGNORE_NEW_LINES),
);

$minX =  INF;
$maxX = -INF;
$maxY = -INF;
array_walk_recursive(
    $input,
    static function (int $value, int $key) use (&$minX, &$maxX, &$maxY): void {
        if ($key % 2 === 0) {
            $minX = min($minX, $value);
            $maxX = max($maxX, $value);
            return;
        }
        $maxY = max($maxY, $value);
    }
);
$cave1 = generateCave($input, $minX, $maxX, $maxY);
$cave1 = simulateSand($cave1, true, [$minX, $maxX, $maxY]);;

// can we calculate minX maxX for part2? yes
// can we be lazy and just use a bigger number? also yes
$cave2 = generateCave($input, $minX - 1000, $maxX + 1000, $maxY + 2);
$cave2 = simulateSand($cave2, false);;

$solution1 = countSand($cave1);
$solution2 = countSand($cave2);

echo "Solution 14-1: $solution1\n";
echo "Solution 14-2: $solution2\n";

function generateCave(array $rocks, int $minX, int $maxX, int $maxY): array
{
    return array_reduce(
        $rocks,
        static function (array $cave, array $rockCoordinates): array {
            while (count($rockCoordinates) > 1) {
                [$from, $to] = array_slice($rockCoordinates, 0, 2);
                array_shift($rockCoordinates);

                if ($from[0] === $to[0]) {
                    $x = $from[0];
                    foreach (range($from[1], $to[1]) as $y) {
                        $cave[$y][$x] = '#';
                    }
                    continue;
                }
                $y = $from[1];
                foreach (range($from[0], $to[0]) as $x) {
                    $cave[$y][$x] = '#';
                }
            }
            return $cave;
        },
        array_fill(0, $maxY, array_fill($minX, $maxX + 1 - $minX, '.'))
    );
}

function simulateSand(array $cave, bool $checkBoundaries, array $boundaries = [null, null, null]): array
{
    $sandPosition = SAND_START_POSITION;
    [$minX, $maxX, $maxY] = $boundaries;

    while (true) {
        [$x, $y] = $sandPosition;

        if ($checkBoundaries && ($y + 1 > $maxY || $x - 1 < $minX || $x + 1 > $maxX)) {
            break;
        }

        if (($cave[$y + 1][$x] ?? '') === '.') {
            $sandPosition[1]++;
            continue;
        }

        if (($cave[$y + 1][$x - 1] ?? '') === '.') {
            $sandPosition[0]--;
            $sandPosition[1]++;
            continue;
        }

        if (($cave[$y + 1][$x + 1] ?? '') === '.') {
            $sandPosition[0]++;
            $sandPosition[1]++;
            continue;
        }

        $cave[$y][$x] = 'o';
        if (!$checkBoundaries && $x === SAND_START_POSITION[0] && $y === SAND_START_POSITION[1]) {
            break;
        }
        $sandPosition = SAND_START_POSITION;
    }

    return $cave;
}

function countSand(array $cave): int
{
    $amountSand = 0;
    array_walk_recursive(
        $cave,
        static function (string $cavePosition) use (&$amountSand) {
            if ($cavePosition === 'o') {
                $amountSand++;
            }
        }
    );
    return $amountSand;
}

function printCave(array $cave): void
{
    foreach ($cave as $line) {
        foreach ($line as $point) {
            echo "$point";
        }
        echo "\n";
    }
}
