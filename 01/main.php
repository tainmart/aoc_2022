<?php

$inputs = array_map(
    static fn (string $input) => explode("\n", $input),
    explode("\n\n", file_get_contents($argv[1]))
);

$sums = array_map(
    static fn(array $elf) => array_sum($elf),
    $inputs
);
rsort($sums);

$solution1 = $sums[0];
$solution2 = array_sum(array_slice($sums, 0, 3));

echo "Solution 01-1: $solution1\n";
echo "Solution 01-2: $solution2\n";
