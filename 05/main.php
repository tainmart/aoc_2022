<?php

[$cratesInput, $instructions] = array_map(
    static fn (string $input) => explode("\n", $input),
    explode("\n\n", file_get_contents($argv[1]))
);

// no need for the last line with only numbers
array_pop($cratesInput);
// might have empty last line
if (empty(end($instructions))) {
    array_pop($instructions);
}

$crates = array_reduce(
    $cratesInput,
    static function (array $result, string $line): array {
        // filter boxes
        $currentCrates = array_filter(
            str_split($line, 4),
            static fn (string $crate) => !empty(trim($crate))
        );
        // format input
        $currentCrates = array_map(
            static fn (string $crate): string => strtr($crate, ['[' => '', ']' => '', '] ' => '']),
            $currentCrates
        );
        // parse into array
        array_walk(
            $currentCrates,
            static function (string $crateValue, int $key) use (&$result): void {
                $result[$key][] = $crateValue;
            }
        );

        return $result;
    },
    array_fill(0, 9, [])
);

$sorted9000 = array_reduce(
    $instructions,
    static fn (array $crates, string $instruction): array => operateCrane($crates, $instruction, true),
    $crates
);

$sorted9001 = array_reduce(
    $instructions,
    static fn (array $crates, string $instruction): array => operateCrane($crates, $instruction, false),
    $crates
);

$solution1 = getOutput($sorted9000);
$solution2 = getOutput($sorted9001);

echo "Solution 05-1: $solution1\n";
echo "Solution 05-2: $solution2\n";

function operateCrane(array $crates, string $instruction, bool $reverse): array
{
    sscanf($instruction, "move %d from %d to %d", $count, $from, $to);
    // fix index
    $from--;
    $to--;

    // move boxes
    $moving = array_splice($crates[$from], 0, $count);
    array_splice($crates[$to], 0, 0, $reverse ? array_reverse($moving) : $moving);

    return $crates;
}

function getOutput(array $sortedCrates): string
{
    return implode('', array_map(
        static fn (array $stack): string => array_shift($stack) ?? '',
        $sortedCrates
    ));
}
