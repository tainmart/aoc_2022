<?php

// add tail optimization or be lazy like me and increase memory
ini_set('memory_limit', '2048M');

$input = array_map(
    static fn (string $line): array => str_split($line),
    file($argv[1], FILE_IGNORE_NEW_LINES)
);


$solution1 = breadthFirstSearch(grid: $input, queue: [getStartingPos($input, 'S')], reverse: false);
$solution2 = breadthFirstSearch(grid: $input, queue: [getStartingPos($input, 'E')], reverse: true);

echo "Solution 12-1: $solution1\n";
echo "Solution 12-2: $solution2\n";

function breadthFirstSearch(
    array $grid,
    array $queue = [],
    array $visited = [],
    int $depth = 0,
    int $nextDepthIncrease = 1,
    bool $reverse = false
): int {
    $position = array_shift($queue);
    $visited[] = $position;

    $childs = getChilds($grid, $position, $visited, $queue, $reverse);
    array_walk(
        $childs,
        static function (string $childPosition) use (&$queue) {
            $queue[] = $childPosition;
        }
    );

    [$x, $y] = explode('x', $position);
    $finishValue = $reverse ? 'a' : 'E';
    if ($grid[$y][$x] === $finishValue) {
        return $depth;
    }

    $nextDepthIncrease--;
    if ($nextDepthIncrease === 0) {
        $depth++;
        $nextDepthIncrease = count($queue);
    }

    return breadthFirstSearch($grid, $queue, $visited, $depth, $nextDepthIncrease, $reverse);
}

function getChilds(array $grid, string $position, array $visited, array $queue, bool $reverse): array
{
    [$x, $y] = explode('x', $position);

    $childs = array_map(
        static fn (array $childPosition): string => implode('x', $childPosition),
        [[$x - 1, $y], [$x + 1, $y], [$x, $y - 1], [$x, $y + 1]]
    );
    return array_reduce(
        $childs,
        static function (array $childs, string $childPosition) use ($grid, $position, $visited, $queue, $reverse): array {
            if (isChildPossible($grid, $position, $childPosition, $visited, $queue, $reverse)) {
                $childs[] = $childPosition;
            }
            return $childs;
        },
        []
    );
}

function isChildPossible(array $grid, string $position, string $childPosition, array $visited, array $queue, bool $reverse): bool
{
    if (in_array($childPosition, $visited) || in_array($childPosition, $queue)) {
        return false;
    }

    $width = count(array_map(null, ...$grid));
    $height = count($grid);
    [$childX, $childY] = array_map('intval', explode('x', $childPosition));

    if (
        $childX < 0 || $childX >= $width ||
        $childY < 0 || $childY >= $height
    ) {
        return false;
    }

    [$x, $y] = explode('x', $position);

    $value = $grid[$y][$x];
    $childValue = $grid[$childY][$childX];

    if ($reverse) {
        $value = $value === 'S' ? 'a' : $value;
        $childValue = $childValue === 'S' ? 'a' : $childValue;

        return ($value === 'E' && in_array($childValue, ['y', 'z'])) ||
            (ctype_lower($value) && ord($value) - ord($childValue) <= 1);
    }

    return ($value === 'S' && in_array($childValue, ['a', 'b'])) ||
        (in_array($value, ['y', 'z']) && $childValue === 'E') ||
        (ctype_lower($childValue) && ord($childValue) - ord($value) <= 1);
}

function getStartingPos(array $input, string $startValue): string
{
    $startingX = null;
    $startingY = -1;
    array_walk_recursive($input, static function (string $value, int $key) use (&$startingX, &$startingY, $startValue) {
        if (null !== $startingX) {
            return;
        }
        if ($key === 0) {
            $startingY++;
        }
        if ($value === $startValue) {
            $startingX = $key;
        }
    });
    return implode('x', [$startingX, $startingY]);
}
