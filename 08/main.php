<?php

$input = array_map(
    static fn (string $line): array => str_split($line),
    file($argv[1], FILE_IGNORE_NEW_LINES)
);

$width = count(transpose($input));
$height = count($input);

[$solution1, $solution2] = array_reduce(
    array_keys($input),
    static function (array $result, int $row) use ($input, $width, $height): array {
        $result = array_reduce(
            array_keys($input[$row]),
            static function (array $innerResult, int $column) use ($row, $input, $width, $height): array {
                if ($row === 0 || $column === 0 || $row === $width - 1 || $column === $height - 1) {
                    $innerResult[0]++;
                    return $innerResult;
                }
                $treeHeight = $input[$row][$column];
                $isTreeVisible = count(array_filter(
                    directionalNeighbours($input, $row, $column),
                    static fn (array $trees): bool => max($trees) < $treeHeight,
                )) > 0;
                if ($isTreeVisible) {
                    $innerResult[0]++;

                    // get neighbours until value
                    $scenicScore = array_product(array_map(
                        static fn (array $array): int => count($array),
                        directionalNeighbours($input, $row, $column, true)
                    ));
                    $innerResult[1] = max($innerResult[1], $scenicScore);
                }
                return $innerResult;
            },
            $result
        );
        return $result;
    },
    [0, 0]
);

echo "Solution 08-1: $solution1\n";
echo "Solution 08-2: $solution2\n";

function transpose(array $array): array
{
    return array_map(null, ...$array);
}

function directionalNeighbours(array $array, int $row, int $column, bool $untilViewBlocked = false): array
{
    $transposed = transpose($array);
    $width = count($transposed);
    $height = count($array);

    $right = array_slice($array[$row], $column + 1);
    $left = array_slice(array_reverse($array[$row]), $width - $column);

    $down = array_slice($transposed[$column], $row + 1);
    $up = array_slice(array_reverse($transposed[$column]), $height - $row);

    if (!$untilViewBlocked) {
        return [$up, $right, $down, $left];
    }

    $currentTreeHeight = $array[$row][$column];
    return array_map(
        static fn (array $neighbours): array => array_reduce(
            $neighbours,
            static function (array $result, int $viewedTreeHeight) use ($currentTreeHeight): array {
                if ($result['blocked']) {
                    return $result;
                }
                if ($viewedTreeHeight >= (int)$currentTreeHeight) {
                    $result['blocked'] = true;
                }
                $result['viewed'][] = $viewedTreeHeight;
                return $result;
            },
            ['viewed' => [], 'blocked' => false]
        )['viewed'],
        [$up, $right, $down, $left]
    );
}
