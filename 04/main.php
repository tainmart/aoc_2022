<?php

$inputs = file($argv[1], FILE_IGNORE_NEW_LINES);

$solution1 = count(
    array_filter(
        $inputs,
        static function (string $line): bool {
            sscanf($line, "%d-%d,%d-%d", $lower1, $upper1, $lower2, $upper2);
            return ($lower1 <= $lower2 && $upper1 >= $upper2) ||
                ($lower2 <= $lower1 && $upper2 >= $upper1);
        }
    )
);

$solution2 = count(
    array_filter(
        $inputs,
        static function (string $line): bool {
            sscanf($line, "%d-%d,%d-%d", $lower1, $upper1, $lower2, $upper2);
            return ($lower1 <= $lower2 && $lower2 <= $upper1) ||
                ($lower2 <= $lower1 && $lower1 <= $upper2);
        }
    )
);


echo "Solution 04-1: $solution1\n";
echo "Solution 04-2: $solution2\n";
