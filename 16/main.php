<?php

$valves = array_reduce(
    file($argv[1], FILE_IGNORE_NEW_LINES),
    static function (array $valves, string $line): array {
        preg_match('/.*(?<valve>[A-Z]{2}).*?(?<flow>\d+).*?(?<tunnels>[A-Z]{2}.*)/', $line, $matches);
        $valves[$matches['valve']] = [
            'flow' => (int)$matches['flow'],
            'tunnels' => explode(', ', $matches['tunnels']),
            'distance' => [$matches['valve'] => 0]
        ];
        return $valves;
    },
    []
);

foreach ($valves as $currentValve => $valveData) {
    foreach ($valveData['tunnels'] as $destinationValve) {
        $valves[$currentValve]['distance'][$destinationValve] = 1;
        addDistances($valves, $currentValve, $destinationValve);
    }
}

printf("finding our own way out\n");
$solution1 = getCosts($valves, 30, 'AA', ['AA'], 0);
printf("elephant is slow, please be patient\n");
$solution2 = getCostsWithAnElefant($valves, [26, 26], ['AA', 'AA'], ['AA'], 0, microtime(true));
printf("\n");

echo "Solution 16-1: $solution1\n";
echo "Solution 16-2: $solution2\n";

function addDistances(array &$valves, string $current, string $starting): void
{
    foreach ($valves[$starting]['tunnels'] as $destination) {
        if ($current === $destination) {
            continue;
        }
        if (empty($valves[$current]['distance'][$destination])) {
            $valves[$current]['distance'][$destination] = $valves[$current]['distance'][$starting] + 1;
            addDistances($valves, $current, $destination);
            continue;
        }
        if ($valves[$current]['distance'][$destination] > ($valves[$current]['distance'][$starting] + 1)) {
            $valves[$current]['distance'][$destination] = $valves[$current]['distance'][$starting] + 1;
            addDistances($valves, $current, $destination);
            continue;
        }
    }
}

function getCosts(array $valves, int $time, string $current, array $visited, int $costs): int
{
    if ($time === 0) {
        return $costs;
    }

    $maxCosts = $costs;
    foreach ($valves[$current]['distance'] as $destination => $distance) {
        if ($distance + 1 > $time) {
            continue;
        }

        $destinationFlow = $valves[$destination]['flow'];
        if ($destinationFlow === 0) {
            continue;
        }

        if (!in_array($destination, $visited)) {
            $destinationTime = $time - 1 - $distance;
            $addableCost = $destinationTime * $destinationFlow;
            $newVisited = $visited;
            $newVisited[] = $destination;
            $destinationCosts = getCosts($valves, $destinationTime, $destination, $newVisited, $costs + $addableCost);
            $maxCosts = max($destinationCosts, $maxCosts);
        }
    }

    return $maxCosts;
}

function getCostsWithAnElefant(array $valves, array $time, array $current, array $visited, int $costs, float $startTime): int
{
    $t = (int)(microtime(true) - $startTime);
    printf("                                                         \r");
    printf("%02d:%02d:%02d, %s\r", $t / 3600, $t / 60 % 60, $t % 60, implode(',', $visited));

    if ($time[0] === 0 && $time[0] === 0) {
        return $costs;
    }

    if ($time[0] < $time[1]) {
        $time = array_reverse($time);
        $current = array_reverse($current);
    }

    $maxCosts = $costs;
    foreach ($valves[$current[0]]['distance'] as $destination1 => $distance1) {
        if ($distance1 + 1 > $time[0]) {
            continue;
        }
        $destinationFlow1 = $valves[$destination1]['flow'];
        if ($destinationFlow1 === 0) {
            continue;
        }
        if (!in_array($destination1, $visited)) {
            $elephantFoundAValve = false;

            $newVisited = $visited;
            $newVisited[] = $destination1;

            foreach ($valves[$current[1]]['distance']  as $destination2 => $distance2) {
                if ($destination1 === $destination2) {
                    continue; // no  hugging
                }
                if ($distance2 + 1 > $time[1]) {
                    continue;
                }
                $destinationFlow2 = $valves[$destination2]['flow'];
                if ($destinationFlow2 === 0) {
                    continue;
                }

                if (!in_array($destination2, $visited)) {
                    $destinationTime1 = $time[0] - 1 - $distance1;
                    $destinationTime2 = $time[1] - 1 - $distance2;

                    $addableCosts = 0;
                    $addableCosts += $destinationTime1 * $destinationFlow1;
                    $addableCosts += $destinationTime2 * $destinationFlow2;

                    $newVisited = $visited;
                    $newVisited[] = $destination1;
                    $newVisited[] = $destination2;

                    $destinationCosts = getCostsWithAnElefant(
                        $valves,
                        [$destinationTime1, $destinationTime2],
                        [$destination1, $destination2],
                        $newVisited,
                        $costs + $addableCosts,
                        $startTime
                    );
                    $maxCosts = max($destinationCosts, $maxCosts);
                    $elephantFoundAValve = true;
                }
            }
            if (!$elephantFoundAValve) {
                $destinationTime1 = $time[0] - 1 - $distance1;
                $addableCosts = $destinationTime1 * $destinationFlow1;

                $destinationCosts = getCostsWithAnElefant(
                    $valves,
                    [$destinationTime1, $time[1]],
                    [$destination1, $current[1]],
                    $newVisited,
                    $costs + $addableCosts,
                    $startTime
                );
                $maxCosts = max($destinationCosts, $maxCosts);
            }
        }
    }

    return $maxCosts;
}
