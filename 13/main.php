<?php

enum ComparisonValue
{
    case Left;
    case Right;
    case Equal;
}

$input1 = array_map(
    static fn (string $block) => array_map(
        static fn (string $array) => json_decode($array),
        explode("\n", $block)
    ),
    explode("\n\n", file_get_contents($argv[1]))
);

$rightOrderPairs = array_filter(
    $input1,
    static function (array $pairs): bool {
        return match (compareItem($pairs[0], $pairs[1])) {
            ComparisonValue::Left => true,
            ComparisonValue::Right => false,
            ComparisonValue::Equal => throw new InvalidArgumentException('lines should not be the same'),
        };
    }
);

$input2 = array_map(
    static fn (string $array) => json_decode($array),
    file($argv[1], FILE_IGNORE_NEW_LINES)
);
$dividerPackets = [[[2]], [[6]]];
$input2[] = $dividerPackets[0];
$input2[] = $dividerPackets[1];
$input2 = array_filter($input2, static fn (?array $array) => null !== $array);

usort(
    $input2,
    static function (array $array1, array $array2): int {
        return match (compareItem($array1, $array2)) {
            ComparisonValue::Left => -1,
            ComparisonValue::Right => 1,
            ComparisonValue::Equal => 0
        };
    }
);

$solution1 = array_sum([...array_keys($rightOrderPairs), count($rightOrderPairs)]);
$solution2 = (array_search($dividerPackets[0], $input2) + 1) * (array_search($dividerPackets[1], $input2) + 1);
echo "Solution 13-1: $solution1\n";
echo "Solution 13-2: $solution2\n";

function compareItem(int|array $item1, int|array $item2): ComparisonValue
{
    if (is_int($item1) && is_int($item2)) {
        return compareInt($item1, $item2);
    }
    if (is_array($item1) && is_array($item2)) {
        return compareArray($item1, $item2);
    }
    if (is_int($item1)) {
        // only item1 is int
        return compareArray([$item1], $item2);
    }
    // only item2 is int
    return compareArray($item1, [$item2]);
}

function compareInt(int $item1, int $item2): ComparisonValue
{
    return match ($item1 <=> $item2) {
        -1 => ComparisonValue::Left,
        1 => ComparisonValue::Right,
        0 => ComparisonValue::Equal,
    };
}

function compareArray(array $item1, array $item2): ComparisonValue
{
    $length = min(count($item1), count($item2));
    for ($key = 0; $key < $length; $key++) {
        $compared = compareItem($item1[$key], $item2[$key]);

        if ($compared === ComparisonValue::Equal) {
            continue;
        }
        return $compared;
    }

    return match (count($item1) <=> count($item2)) {
        -1 => ComparisonValue::Left,
        1 => ComparisonValue::Right,
        0 => ComparisonValue::Equal,
    };
}
