<?php

const SHAPE_SCORE = ['X' => 1, 'Y' => 2, 'Z' => 3];
const GAME_WINS = ['A' => 'Y', 'B' =>  'Z', 'C' =>  'X'];
const GAME_DRAWS = ['A' =>  'X', 'B' =>  'Y', 'C' =>  'Z'];
const GAME_LOSS = ['A' =>  'Z', 'B' =>  'X', 'C' =>  'Y'];
const STRATEGY = ['X' => GAME_LOSS, 'Y' => GAME_DRAWS, 'Z' => GAME_WINS];


$inputs = array_map(
    static fn (string $line) => explode(' ', $line),
    file($argv[1], FILE_IGNORE_NEW_LINES)
);

$scores1 = array_map(
    static fn (array $game) => getScore($game[1], $game[0]),
    $inputs
);

$scores2 = array_map(
    static fn (array $game) => getScore(STRATEGY[$game[1]][$game[0]], $game[0]),
    $inputs
);

$solution1 = array_sum($scores1);
$solution2 = array_sum($scores2);

echo "Solution 02-1: $solution1\n";
echo "Solution 02-2: $solution2\n";

function getScore(string $self, string $opponent): int
{
    $score = SHAPE_SCORE[$self];
    if (GAME_WINS[$opponent] === $self) {
        $score += 6;
        return $score;
    }
    if (GAME_DRAWS[$opponent] === $self) {
        $score += 3;
        return $score;
    }
    return $score;
}
