<?php

$inputs = array_map(
    static fn (string $line): array => str_split($line),
    file($argv[1], FILE_IGNORE_NEW_LINES)
);

$scores1 = array_map(
    static function (array $backpack): int {
        [$compartment1, $compartment2] = array_chunk($backpack, ceil(count($backpack) / 2));
        $char = getCommonChar($compartment1, $compartment2);
        return getPriorityValue($char);
    },
    $inputs
);

$scores2 = array_map(
    static function (array $backpacks): int {
        $char = getCommonChar(...$backpacks);
        return getPriorityValue($char);
    },
    array_chunk($inputs, 3)
);

$solution1 = array_sum($scores1);
$solution2 = array_sum($scores2);

echo "Solution 03-1: $solution1\n";
echo "Solution 03-2: $solution2\n";

function getCommonChar(array ...$arrays): string
{
    return array_values(array_intersect(...$arrays))[0];
}

function getPriorityValue(string $char): int
{
    return ctype_upper($char) ? ord($char) - 38 : ord($char) - 96;
}
