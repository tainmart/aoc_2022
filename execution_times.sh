#!/usr/bin/env bash
TIMEFORMAT=%R

for directory in *; do
  if [ -d "${directory}" ]; then
    echo -n "Day $directory: "
    time (php $directory/main.php $directory/input.txt > /dev/null)
  fi
done