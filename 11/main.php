<?php

enum Operator
{
    case Multiplication;
    case Addition;
}

class Monkey
{
    public function __construct(
        protected array $items,
        protected Operator $operator,
        protected ?int $operationValue,
        public int $testValue,
        protected int $throwTrueTo,
        protected int $throwFalseTo,
        protected ?int $currentItem = null,
        public int $inspectedItems = 0,
    ) {
    }

    public static function parseFromInput(array $monkeyInput): self
    {
        preg_match_all("/\d+/", $monkeyInput[1], $matchedItems);
        return new self(
            items: $matchedItems[0],
            operator: str_contains($monkeyInput[2], '*') ? Operator::Multiplication : Operator::Addition,
            operationValue: (filter_var($monkeyInput[2], FILTER_SANITIZE_NUMBER_INT)) ?: null,
            testValue: (int)filter_var($monkeyInput[3], FILTER_SANITIZE_NUMBER_INT),
            throwTrueTo: (int)filter_var($monkeyInput[4], FILTER_SANITIZE_NUMBER_INT),
            throwFalseTo: (int)filter_var($monkeyInput[5], FILTER_SANITIZE_NUMBER_INT),
        );
    }

    public function turn(array $monkies, int $lowestCommonMultiplier, bool $withWorry): void
    {
        array_walk(
            $this->items,
            function () use ($monkies, $lowestCommonMultiplier, $withWorry): void {
                $this->currentItem = array_shift($this->items);
                $this->inspect();
                if ($withWorry) {
                    $this->reduceWorry();
                }
                $this->currentItem %= $lowestCommonMultiplier;
                $this->throw($monkies);
            }
        );
    }

    public function addItem(int $item): void
    {
        $this->items[] = $item;
    }

    protected function inspect(): void
    {
        $operationValue = $this->operationValue ?? $this->currentItem;
        $this->currentItem = match ($this->operator) {
            Operator::Multiplication => ($this->currentItem * $operationValue),
            Operator::Addition => $this->currentItem + $operationValue,
        };

        $this->inspectedItems++;
    }

    protected function reduceWorry(): void
    {
        $this->currentItem = floor($this->currentItem / 3);
    }

    protected function throw(array $monkies): void
    {
        if ($this->currentItem % $this->testValue === 0) {
            $monkies[$this->throwTrueTo]->addItem($this->currentItem);
        } else {
            $monkies[$this->throwFalseTo]->addItem($this->currentItem);
        }
    }
}

$input = array_map(
    static fn (string $block) => array_map('trim', explode("\n", $block)),
    explode("\n\n", file_get_contents($argv[1]))
);


$solution1 = mostInspectedItemsAfterRounds($input, 20, true);
$solution2 = mostInspectedItemsAfterRounds($input, 10_000, false);

echo "Solution 11-1: $solution1\n";
echo "Solution 11-2: $solution2\n";

function mostInspectedItemsAfterRounds(array $input, int $amountRounds, bool $withWorry): int
{
    $monkies = array_map(
        static fn (array $monkeyInput): Monkey => Monkey::parseFromInput($monkeyInput),
        $input
    );

    $lowestCommonMultiplier = array_reduce(
        $monkies,
        static fn (int $lowestCommonMultiplier, Monkey $monkey): int => $lowestCommonMultiplier * $monkey->testValue,
        1
    );

    $rounds = array_fill(0, $amountRounds, 0);
    array_walk(
        $rounds,
        static fn () => array_walk(
            $monkies,
            static fn (Monkey $monkey) => $monkey->turn($monkies, $lowestCommonMultiplier, $withWorry)
        )
    );

    $inspectedItems = array_map(
        static fn (Monkey $monkey): int => $monkey->inspectedItems,
        $monkies
    );
    rsort($inspectedItems);

    return array_product(array_slice($inspectedItems, 0, 2));
}
