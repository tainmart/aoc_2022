<?php

const FILESYSTEM_SIZE = 70_000_000;
const UPDATE_SIZE = 30_000_000;

$filesystem = array_reduce(
    file($argv[1], FILE_IGNORE_NEW_LINES),
    static function (array $result, string $line): array {
        // determine where we currently are
        sscanf($line, '$ cd %s', $directory);
        if (null !== $directory) {
            if ($directory === '/') {
                // root
                $result['pwd'] = '/';
            } else if ($directory === '..') {
                // go up one directory
                $result['pwd'] = dirname($result['pwd']);
            } else {
                $result['pwd'] = sprintf('%s/%s', rtrim($result['pwd'], '/') , $directory);
            }

            if (!array_key_exists($result['pwd'], $result['du'])) {
                $result['du'][$result['pwd']] = 0;
            }
            
            return $result;
        }

        // add files to where ever we currently are
        sscanf($line, '%d %s', $fileSize, $_fileName);
        if (null !== $fileSize) {
            array_walk(
                $result['du'],
                static function (int $_dirSize, string $path) use ($fileSize, $result): void {
                    if (str_contains($result['pwd'], $path)) {
                        $result['du'][$path] += $fileSize;
                    }
                }
            );
        }

        return $result;
    },
    [
        'pwd' => '',
        'du' => [],
    ]
);

$unused = FILESYSTEM_SIZE - $filesystem['du']['/'];
$requiredForUpdate = UPDATE_SIZE - $unused;

$solution1 = array_sum(array_filter($filesystem['du'], static fn (int $dirSize) => $dirSize < 100_000));
$solution2 = array_reduce(
    $filesystem['du'],
    static function (int $result, int $size) use ($requiredForUpdate): int {
        return $size > $requiredForUpdate ? min($result, $size) : $result;
    },
    FILESYSTEM_SIZE
);

echo "Solution 07-1: $solution1\n";
echo "Solution 07-2: $solution2\n";
