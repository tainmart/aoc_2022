<?php

$instructions = file($argv[1], FILE_IGNORE_NEW_LINES);

['signal' => $solution1, 'crt' => $solution2] = array_reduce(
    $instructions,
    static function (array $result, string $instruction): array {
        $result['cycle']++;
        $result['signal'] += calculateSignal($result['cycle'], $result['registerX']);
        $result['crt'] = drawPixel($result['cycle'], $result['registerX'], $result['crt']);

        sscanf($instruction, '%s %d', $command, $value);

        if ($command === 'noop') {
            return $result;
        }

        if ($command === 'addx') {
            $result['cycle']++;
            $result['signal'] += calculateSignal($result['cycle'], $result['registerX']);
            $result['crt'] = drawPixel($result['cycle'], $result['registerX'], $result['crt']);
            $result['registerX'] += $value;

            return $result;
        }

        return $result;
    },
    ['registerX' => 1, 'cycle' => 0, 'signal' => 0, 'crt' => array_fill(0, 240, '.')]
);


echo "Solution 10-1: $solution1\n";
echo "Solution 10-2:\n";
array_walk(
    $solution2,
    static function(string $pixel, int $key): void {
        if ($key > 0 && $key % 40 === 0) {
            echo "\n";
        }
        echo "$pixel";      
    }
);

function calculateSignal(int $cycle, int $registerX): int
{
    return $cycle % 40 === 20
        ? $cycle * $registerX
        : 0;
}

function drawPixel(int $cycle, int $registerX, array $crt): array
{
    $crtPosition = ($cycle - 1) % 40 + 1;
    if ($crtPosition >= $registerX && $crtPosition <= $registerX + 2) {
        $crt[$cycle - 1] = '#';
    }
    return $crt;
}
