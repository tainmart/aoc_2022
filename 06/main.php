<?php

$input = trim(file_get_contents($argv[1]));

$differentFour = array_reduce(
    str_split($input),
    static function (array $result, string $char): array {
        if (count($result) === 4) {
            return $result;
        }
        return checkMarker($result, $char);
    },
    []
);

$differentFourteen = array_reduce(
    str_split($input),
    static function (array $result, string $char): array {
        if (count($result) === 14) {
            return $result;
        }
        return checkMarker($result, $char);
    },
    []
);

$solution1 = strpos($input, implode('', $differentFour)) + 4;
$solution2 = strpos($input, implode('', $differentFourteen)) + 14;

echo "Solution 06-1: $solution1\n";
echo "Solution 06-2: $solution2\n";

function checkMarker(array $result, string $char): array
{
    $currentCharPosition = array_search($char, $result);
    if ($currentCharPosition !== false) {
        array_splice($result, 0, $currentCharPosition + 1);
    }
    $result[] = $char;

    return $result;
}
