<?php

error_reporting(0);

$input = file($argv[1], FILE_IGNORE_NEW_LINES);

$solution1 = simluateRopeForUniqueTailPositions($input, array_fill(0,  2, ['x' => 0, 'y' => 0]));
$solution2 = simluateRopeForUniqueTailPositions($input, array_fill(0, 10, ['x' => 0, 'y' => 0]));

echo "Solution 09-1: $solution1\n";
echo "Solution 09-2: $solution2\n";

function simluateRopeForUniqueTailPositions(array $input, array $startingPositions): int
{
    $tailVisited = [end($startingPositions)];

    array_reduce(
        $input,
        static function (array $currentPositions, string $motion) use (&$tailVisited): array {
            sscanf($motion, '%s %d', $direction, $steps);
            array_walk(
                array_fill(0, $steps, 0),
                static function () use ($direction, &$currentPositions, &$tailVisited): void {
                    $currentPositions[0] = updateHead($currentPositions[0], $direction);
                    array_walk(
                        array_fill(0, count($currentPositions) - 1, 0),
                        static function ($_, $key) use (&$currentPositions, &$tailVisited): void {
                            $head = $key;
                            $tail = $key + 1;
                            $currentPositions[$tail] = updatetail($currentPositions[$head], $currentPositions[$tail]);

                            if ($tail === count($currentPositions) - 1) {
                                $tailVisited[] = $currentPositions[$tail];
                            }
                        }
                    );
                }
            );
            return $currentPositions;
        },
        $startingPositions
    );

    $tailVisitedString = array_map(
        static fn (array $position) => sprintf('%s-%s', $position['x'], $position['y']),
        $tailVisited
    );
    return count(array_unique($tailVisitedString));
}

function updateHead(array $position, string $direction): array
{
    return match ($direction) {
        'U' => ['x' => $position['x'], 'y' => $position['y'] + 1],
        'D' => ['x' => $position['x'], 'y' => $position['y'] - 1],
        'R' => ['x' => $position['x'] + 1, 'y' => $position['y']],
        'L' => ['x' => $position['x'] - 1, 'y' => $position['y']],
    };
}

function updateTail(array $positionHead, array $positionTail): array
{
    $dx = $positionHead['x'] - $positionTail['x'];
    $dy = $positionHead['y'] - $positionTail['y'];

    if (abs($dx) > 1 || abs($dy) > 1) {
        $positionTail['x'] += $dx === 0 ? 0 : $dx / abs($dx);
        $positionTail['y'] += $dy === 0 ? 0 : $dy / abs($dy);
    }

    return $positionTail;
}
